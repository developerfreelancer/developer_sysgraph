#!/usr/bin/env python
#coding:utf-8

import json

def createDefaultConf():
    settings = {
        "host_name":"server_pesquisa",
        "host":"191.52.62.240",
        "port":"7070",
        "generation_graphif":["day","week","mounth","year"],
        "refresh":"180",
        "work_dir":"project_path",
        "work_data_base":{
                "name":"server_pesquisa",
                "user":"server_pesquisa",
                "password":"server_pesquisa_123",
                "host":"localhost",
                "port":"27017"},
        "services":["cpu","memory","swap","users","update",
            {"data_base":{
                "name":"db_name",
                "user":"user_name",
                "password":"db_peswd",
                "host":"localhost",
                "port":"12345",
                "services":[
                    "users",
                    "size"
                    ]
                }
            }
        ]
    }
    settings = json.dumps(settings,indent=4, separators=(',', ':'))
    configurations = open("server_pesquisa.cfg","w")
    configurations.write(settings)
    configurations.close()

if __name__ == '__main__':
    createDefaultConf()
