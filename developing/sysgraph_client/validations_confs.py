#!/usr/bin/env python
#coding:utf-8

def thereAreSettingInConf_d(config):
    if not config:
        erro_messenger = 'Not Found config in dir conf.d, please verify'
        print(erro_messenger)
        return 1
    else:
        return 0

def validationRefresh(validations_error,config):

        
    if float(config['refresh']) < 180.0:
        erro_messenger = 'Error in the refresh of %s, choose value above 180 secundes.' %(config['host_name'])
        print (erro_messenger)
        validations_error.append(1)
    else:
        validations_error.append(0)
            
def runValidations(config):
    validations_error = []
    validationRefresh(validations_error,config)
    return validations_error
    
