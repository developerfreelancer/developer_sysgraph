#!/usr/bin/env python
#coding:utf-8

from datetime import datetime
import psutil

def MEMORY():
    memory = {}
    status = ['total', 'available', 'percent', 'used', 'free', 'active', 'inactive', 'buffers', 'cached','shared']
    results_memory = psutil.virtual_memory()
    for i, result in enumerate(results_memory):
        memory[status[i]]=(str(result).strip('L'))
    memory['datetime'] = str(datetime.now())
    return memory