#!/usr/bin/env python
#coding:utf-8

from datetime import datetime
import psutil

def SWAP():
    swap = {}
    status = ['total','used', 'free','percent', 'sin', 'sout']
    results_swap = psutil.swap_memory()
    for i, result in enumerate(results_swap):
        swap[status[i]]=(str(result).strip('L'))
    swap['datetime'] = str(datetime.now())
    return swap
