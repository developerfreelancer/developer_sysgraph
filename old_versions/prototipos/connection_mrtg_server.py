#coding:utf-8
#!/user/bin/python #

import socket 
tcp_ip = '127.0.0.1' 
tcp_port = 7000 

socket_tcp_ip = (tcp_ip, tcp_port) 
'''Conjunto de IP e Porta'''

serv_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
'''Nesta, criamos o nosso mecanismo de Socket para receber a conexão, onde 
	na função passamos 2 argumentos, AF_INET que declara a família do protocolo; 
	se fosse um envio via Bluetooth por exemplo, seria: AF_BLUETOOTH, SOCKET_STREAM, indica que será TCP/IP. '''

serv_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) 
''' Essa linha serve para zerar o TIME_WAIT do Socket, por exemplo, se o 
	programa estiver aguardando uma conexão e você der CTRL+C para 
	interromper, o programa fecha porém o Socket continua na escuta '''

serv_socket.bind(socket_tcp_ip) 
'''Esta linha define para qual IP e porta o servidor deve aguardar a conexão, 
	que no nosso caso é qualquer IP, por isso o Host é '''

serv_socket.listen(10) 
''' Define o limite de conexões'''

print 'aguardando conexao' 

con, cliente = serv_socket.accept() 
'''Deixa o Servidor na escuta aguardando as conexões'''

print 'conectado' 
print "aguardando mensagem"
 
recebe = con.recv(1024)
'''Que aguarda um dado enviado pela rede de até 1024 Bytes, a função 'recv' 
	possui somente 1 argumento que é o tamanho do Buffer. '''

print "mensagem recebida:" + recebe 

serv_socket.close()
