#!/usr/bin/env python
#coding:utf-8

from socket import *
from scripts_mrtg import *
import os

def serverSocket():
	udp_ip = '' 
	udp_port = 7070 
	return (udp_ip, udp_port)

def startServerSocket(server_socket):
	server_socket = socket(AF_INET,SOCK_DGRAM)
	server_socket.bind(('',7070))
	return server_socket
	
def responseMessengerMrtgServer(server_connections):
	while True:
		messenger, cliente_address = server_connections.recvfrom(1024)
		
		if (messenger == 'CPU'):
			messenger =CPU()
		elif(messenger == 'MEMORY'):
			messenger =MEMORY()
		elif(messenger == 'SWAP'):
			messenger =SWAP()
		else:
			messenger = 'Comand Not Found'

		server_connections.sendto(messenger,cliente_address)
		
def runMrtgServer():
	server_socket = serverSocket()
	server_connections = startServerSocket(server_socket)
	responseMessengerMrtgServer(server_connections)

if __name__ == '__main__':
	runMrtgServer()

