#!/usr/bin/env python
#coding:utf-8

import psutil
import json

def MEMORY():
	memory = {}
	status = ['total', 'available', 'percent', 'used', 'free', 'active', 'inactive', 'buffers', 'cached']
	results_memory = psutil.virtual_memory()
	for i, result in enumerate(results_memory):
		memory[status[i]]=(str(result).strip('L')) + ' Bytes' 
	return json.dumps(memory)
