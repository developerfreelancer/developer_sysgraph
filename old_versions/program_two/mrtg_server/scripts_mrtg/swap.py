import psutil
import json

def SWAP():
	swap = {}
	status = ['total','used', 'free','percent', 'sin', 'sout']
	results_swap = psutil.swap_memory()
	for i, result in enumerate(results_swap):
		swap[status[i]]=(str(result).strip('L')) + ' Bytes' 
	return json.dumps(swap)
