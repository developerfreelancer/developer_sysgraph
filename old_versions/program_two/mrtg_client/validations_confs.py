#!/usr/bin/env python
#coding:utf-8

def validationRefresh(config):
	if float(config['refresh']) < 30.0:
		erro_messenger = 'Error in the refresh of %s, choose value above 180 secundes.' %(config['host_name'])
		print erro_messenger
		return 1
	return 0
			
def runValidations(config):
	return validationRefresh(config)
	
