#!/usr/bin/env python
#coding:utf-8

import psutil
import json

def CPU():
	carga = {}
	carga['utilizacao_total'] = psutil.cpu_percent(interval=1)
	carga['livre'] = 100 - carga['utilizacao_total']
	
	result = psutil.cpu_percent(interval=1,percpu=True)
	for i in range(len(result)):
		carga['cpu[%d]' %(i)]= result[i]
	
	return(json.dumps(carga))
