#!/usr/bin/env python
#coding:utf-8

from socket import *
import subprocess
import os
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


def serverSocket():
	udp_ip = '' 
	udp_port = 7070 
	return (udp_ip, udp_port)

def startServerSocket(server_socket):
	server_socket = socket(AF_INET,SOCK_DGRAM)
	server_socket.bind(('',7070))
	return server_socket
	
def responseMessengerMrtgServer(server_connections):
	while True:
		messenger, cliente_address = server_connections.recvfrom(1024)
		command =  os.path.join(BASE_DIR,('mrtg_server/scripts_mrtg/'+messenger))
		messenger = subprocess.check_output(command, stderr=subprocess.STDOUT)
		server_connections.sendto(messenger.strip('\n'),cliente_address)
		
def runMrtgServer():
	server_socket = serverSocket()
	server_connections = startServerSocket(server_socket)
	responseMessengerMrtgServer(server_connections)

if __name__ == '__main__':
	runMrtgServer()

