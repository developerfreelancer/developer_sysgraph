#!/usr/bin/env python
#coding:utf-8

from socket import *
import sys
    
def serverSocket(udp_ip='127.0.0.1', udp_port=7070):
	return (udp_ip, int(udp_port))

def startClientSocket():
	return socket(AF_INET,SOCK_DGRAM)
	
def sendMessengerMrtgClient(client_connections, server_socket, messenger):
	client_connections.sendto(messenger,server_socket)

def printResult(client_connections):
	client_connections.settimeout(5.0)
	messenger, serverAddress = client_connections.recvfrom(1024)
	print messenger
	
def runMrtgClient(argv):
	server_socket = serverSocket(argv[1], argv[2])
	client_connections = startClientSocket()
	sendMessengerMrtgClient(client_connections,server_socket,argv[3])
	printResult(client_connections)
	
if __name__ == '__main__':
	if len(sys.argv) >= 5:
		print sys.argv[0]
	runMrtgClient(sys.argv)
	
