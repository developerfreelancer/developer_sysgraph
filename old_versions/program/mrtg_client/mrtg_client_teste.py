#!/usr/bin/env python
#coding:utf-8

from socket import *
import json
import sys
    
def serverSocket(udp_ip='127.0.0.1', udp_port=7070):
	return (udp_ip, int(udp_port))

def startClientSocket():
	return socket(AF_INET,SOCK_DGRAM)
	
def sendMessengerMrtgClient(client_connections, server_socket, commands):

	for command in commands.replace(' ','').split(','):
		client_connections.sendto(command,server_socket)
		printResult(client_connections)

def printResult(client_connections):
	client_connections.settimeout(5.0)
	messenger, serverAddress = client_connections.recvfrom(1024)
	
	try:
		messenger = json.loads(messenger)
	except:
		pass
		
	print messenger
	
def runMrtgClient(config):
	server_socket = serverSocket(config['host'], int(config['port']))
	client_connections = startClientSocket()
	sendMessengerMrtgClient(client_connections,server_socket,config['services'])
