#!/usr/bin/env python
#coding:utf-8

from glob import glob
from mrtg_client_teste import runMrtgClient
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

def openConfigurations():
	configurations = []
	
	for file in glob(os.path.join(BASE_DIR,'mrtg_client/conf_d/*.cfg')):
		args = {}
		for line in (open(file,'r').readlines()):
			args[line[:line.find(':')]] = ((line[line.find(':')+1:].strip()))
			
		configurations.append(args)
	return configurations
	
def runDaemonPlugin():
	configurations = openConfigurations()
	for config in configurations:
		runMrtgClient(config)

if __name__ == '__main__':
	runDaemonPlugin()
